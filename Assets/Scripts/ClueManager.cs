﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

[Serializable]
public class Clue
{
    public string clueString;
    
    public Clue(string s)
    {
        clueString = s;
    }
}
public class ClueManager : MonoBehaviour {

    private List<ClueHolder> clueHolders;
    [SerializeField]
    private List<Clue> clues;
    [SerializeField]
    private Clue nothingItem;
    

    public void AddItem(ClueHolder clueHolder)
    {
        clueHolders.Add(clueHolder);
    }

    public void AssignClues(List<Clue> clues)
    {
        for (int i = 0; i < clues.Count; i++)
        {
            ClueHolder holder = clueHolders[UnityEngine.Random.Range(0, clueHolders.Count - 1)];
            holder.clue = clues[i];
            clueHolders.Remove(holder);
        }
    }

    public ClueManager()
    {
        clueHolders = new List<ClueHolder>();
    }
}
