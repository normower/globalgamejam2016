using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public enum ItemType
{
    Nothing,
    Egg,
    Watermeloan,
    Cactus,
    Sweet
}

[Serializable]
public class Item
{
    public ItemType type;
    public Sprite image;
}


public class ItemManager : MonoBehaviour
{
    private List<ItemHolder> itemHolders;
    [SerializeField]
    private List<Item> items;
    public Item nothingItem;
    

    public void AddItem(ItemHolder itemHolder)
    {
        if (itemHolders == null)
            itemHolders = new List<ItemHolder>();
        itemHolders.Add(itemHolder);
    }

    public void AssignItems()
    {
        if (itemHolders == null)
            return;

        List<Item> itemsToPlace = new List<Item>(items);

        for (int i = 0; i < itemHolders.Count - items.Count; i++)
        {
            itemsToPlace.Add(nothingItem);
        }

        itemsToPlace.Shuffle<Item>();

        for (int i = 0; i < itemHolders.Count; i++)
        {
            itemHolders[i].item = itemsToPlace[i];
        }
    }

    public void Awake()
    {
        itemHolders = new List<ItemHolder>();
    }
    
    public ItemType ReturnRandomItemType()
    {
        var v = ItemType.GetValues(typeof(ItemType));
        return (ItemType)v.GetValue(new System.Random().Next(v.Length));
    }
}




