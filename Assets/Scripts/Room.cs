﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Room : MonoBehaviour
{
    public RoomType roomType;
    private List<Player> playersInRoom;

    public List<Player> PlayersInRoom
    {
        get { return playersInRoom; }
    }

    public void AddPlayer(Player player)
    {
        if (playersInRoom == null)
            playersInRoom = new List<Player>();

        if (!playersInRoom.Contains(player))
            playersInRoom.Add(player);
    }

    public void RemovePlayer(Player player)
    {
        if (playersInRoom.Contains(player))
            playersInRoom.Remove(player);
    }

    private void Awake()
    {
        playersInRoom = new List<Player>();
        GameManager.Instance.RoomManager.AddRoom(this);
    }
}
