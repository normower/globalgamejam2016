﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;



public enum TaskType
{
    Variable,
    Information,
    Time
}
public class Task
{
    public TaskType currentTaskType;
    
}

public class VariableTask : Task
{
    public ItemType item;
    public PlayerType player;
    public RoomType room;
    public VariableTask(ItemType i, PlayerType p, RoomType r)
    {
        item = i;
        player = p;
        room = r;
    }
}


public class TaskManager{
    public Task CreateTask(int i)
    {        
        Task temp;
        switch(i)
        {
            case 0:
            //TODO: Random item picking
                temp = new VariableTask(GameManager.Instance.ItemManager.ReturnRandomItemType(), 
                GameManager.Instance.TurnManager.ReturnRandomPlayerType(),
                GameManager.Instance.RoomManager.ReturnRandomRoomType());
                break;
            case 1:
                temp = new Task();
                break;
            case 2:
                temp = new Task();
                break;
            default:
                temp = new Task();
                break;
        }
        return temp;
    }
    
    public List<Clue> CreateCluesForTask(Task t)
    {
        List<Clue> clueList = new List<Clue>();
        switch(t.currentTaskType)
        {
            case TaskType.Variable:
                VariableTask vt = (VariableTask)t;
                clueList.Add(new Clue("The item you need is the " + vt.item.ToString()));
                clueList.Add(new Clue("The room you need is the " + vt.room.ToString()));
                clueList.Add(new Clue("The player you need is the " + vt.player.ToString()));
            break;
        }
        return clueList;
    }
    
    public bool CheckTaskComplete(Task t)
    {
        bool b = false;
        switch(t.currentTaskType)
        {
            case TaskType.Variable:
                VariableTask vt = (VariableTask)t;
                if(GameManager.Instance.RoomManager.IsItemInRoom(vt.room, vt.item) &&
                GameManager.Instance.TurnManager.IsPlayerInRoom(vt.player,vt.room))
                {
                    b = true;
                }
                break;
        }
        
        return b;
        //TODO: Check that tasks can be completed.
    }
}
