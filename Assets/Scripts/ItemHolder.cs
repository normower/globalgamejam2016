﻿using UnityEngine;
using System.Collections;

public class ItemHolder : MonoBehaviour
{
    public Item item;

    private void Awake()
    {
        GameManager.Instance.ItemManager.AddItem(this);
    }
    
    public void PickUpItem()
    {
        if (item.type != ItemType.Nothing)
            GameManager.Instance.TurnManager.ReturnCurrentPlayer().Inventory.Add(item);

        GameManager.Instance.UIManager.ShowPlayerFeedback("Picked Up " + item.type.ToString());
        GameManager.Instance.UIManager.PopulateInventoryUI();
        item = GameManager.Instance.ItemManager.nothingItem;
    }
}
