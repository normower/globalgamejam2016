﻿using UnityEngine;
using System.Collections;

public interface IEvent
{
    void Update();
    void AddEvent();
    void RemoveEvent();	
}
