﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


    
public enum RoomType
{
    Entrance,
    Kitchen,
    Hallway,
    Bathroom,
    Lounge,
    DiningRoom,
    BookRoom,
    Gaming,
    DressingRoom
}

public class RoomManager
{
    private List<Room> roomList;	
    
    public RoomType ReturnRandomRoomType()
    {
        var v = RoomType.GetValues(typeof(RoomType));
        RoomType temp = RoomType.Hallway;
        while (temp == RoomType.Hallway)
        {
            temp = (RoomType)v.GetValue(new System.Random().Next(v.Length));
        }
        return temp;
    }

    public Room FindRoom(RoomType type)
    {
        for (int i = 0; i < roomList.Count; i++)
        {
            if (roomList[i].roomType == type)
                return roomList[i];
        }

        return null;
    }
    
    public bool IsItemInRoom(RoomType r, ItemType i )
    {
        bool b = false;
        Room room;
        foreach(Room rm in roomList)
        {
            if(rm.roomType == r)
            {
                room = rm;
                for(int j = 0; j < room.PlayersInRoom.Count; j++)
                {
                    foreach(Item it in room.PlayersInRoom[j].Inventory)
                    {
                        if(it.type == i)
                        {
                            b = true;
                        }
                    }
                }
                
            }
        }
        
        
        return b;
    }

    public void AddRoom(Room room)
    {
        if (roomList == null)
            roomList = new List<Room>();
        roomList.Add(room);
    }

    public RoomManager()
    {
        roomList = new List<Room>();
    }
}
