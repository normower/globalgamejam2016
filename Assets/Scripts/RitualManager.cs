﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class Ritual
{
    List<Task> taskList = new List<Task>();
    List<Task> counterTaskList = new List<Task>();
    public List<Task> TaskList
    {
        get{return taskList;}
    }
    public List<Task> CounterTaskList
    {
        get{return counterTaskList;}
    }
    public Ritual()
    {        
        taskList.Add(GameManager.Instance.TaskManager.CreateTask(0));
        counterTaskList.Add(GameManager.Instance.TaskManager.CreateTask(0));        
        GameManager.Instance.ClueManager.AssignClues(GameManager.Instance.TaskManager.CreateCluesForTask(counterTaskList[0]));
        Debug.Log("Created Ritual.");
        /*for(int i = 0; i < 3; i++)
        {
            taskList.Add(GameManager.Instance.TaskManager.CreateTask(i));
            counterTaskList.Add(GameManager.Instance.TaskManager.CreateCounterTask(taskList[i]));
            GameManager.Instance.TaskManager.CreateCluesForCounterTask(clueList, counterTaskList[i]);
        }*/
    }
}

public class RitualManager
{    
    private Ritual currentRitual;
    public void CreateRitual(Player p)
    {
        currentRitual = new Ritual();
        p.Possessed = true;
        p.Clues.AddRange(GameManager.Instance.TaskManager.CreateCluesForTask(currentRitual.TaskList[0]));
        p.Clues.Add(new Clue("You Must Complete The Ritual!"));
    }
    public Ritual CurrentRitual
    {
        get { return currentRitual; }
    }
    
}
