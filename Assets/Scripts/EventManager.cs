﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class EventManager
{
    private List<IEvent> events;

    public void RegisterEvent(IEvent newEvent)
    {
        events.Add(newEvent);
    }

    public void DeregisterEvent(IEvent oldEvent)
    {
        if (events.Contains(oldEvent))
            events.Remove(oldEvent);
    }
	
    public void Update()
    {
        for (int i = 0; i < events.Count; i++)
        {
            events[i].Update();
        }
    }

    public EventManager()
    {
        events = new List<IEvent>();
    }
}
