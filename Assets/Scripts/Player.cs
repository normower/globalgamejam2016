﻿using System.Collections;
using System.Collections.Generic;

    public enum PlayerType
    {
        Jock, //Chad
        Slut, //Scott Redrup @BrainyBeard
        Joker, //Joker
        Virgin //Mary
    }
public class Player {
 
    private RoomType currentRoomType;
    private Room currentRoom;

    public RoomType CurrentRoomType
    {
        get{return currentRoomType;}
        set{currentRoomType = value;}
    }

    public Room CurrentRoom
    {
        get { return currentRoom; }
        set
        {
            currentRoom = value;
            currentRoomType = value.roomType;
        }
    }

    private PlayerType thisPlayerType;
    public PlayerType ThisPlayerType
    {
        get{ return thisPlayerType; }
        set { thisPlayerType = value; }
    }
    List<Item> inventory;
    List<Clue> clues;
    public List<Item> Inventory
    {
        get{ return inventory; }
    }

    public List<Clue> Clues
    {
        get { return clues; }
    }
    
    private bool possessed = false;
    public bool Possessed
    {
        get { return possessed; }
        set { possessed = value; }
    }
    
    public Player(RoomType t, PlayerType pt)
    {
        currentRoomType = t;
        thisPlayerType = pt;
        inventory = new List<Item>();
        clues = new List<Clue>();
        currentRoom = GameManager.Instance.RoomManager.FindRoom(t);
        currentRoom.AddPlayer(this);
    }
    
    
}
  
    //Location, Inventory
    

