﻿using UnityEngine;
using System.Collections;
using System;

public class EventChecker : IEvent
{
    private Func<bool> isComplete;
    private Action postEvent;

    public void AddEvent()
    {
        GameManager.Instance.EventManager.RegisterEvent(this);
    }

    public void RemoveEvent()
    {
        GameManager.Instance.EventManager.DeregisterEvent(this);
    }

    public void Update()
    {
        if (isComplete())
        {
            if (postEvent != null)
                postEvent();
            RemoveEvent();
        }
    }

    public EventChecker(Func<bool> isComplete, Action postEvent)
    {
        this.isComplete = isComplete;
        this.postEvent = postEvent;
    }
}

public class EventChecker<T> : IEvent
{
    private Func<T, bool> isComplete;
    private Action<T> postEvent;
    private T parameter;

    public void AddEvent()
    {
        GameManager.Instance.EventManager.RegisterEvent(this);
    }

    public void RemoveEvent()
    {
        GameManager.Instance.EventManager.DeregisterEvent(this);
    }

    public void Update()
    {
        if (isComplete(parameter))
        {
            if (postEvent != null)
                postEvent(parameter);
            RemoveEvent();
        }
    }

    public EventChecker(Func<T, bool> isComplete, Action<T> postEvent, T parameter)
    {
        this.isComplete = isComplete;
        this.postEvent = postEvent;
        this.parameter = parameter;
    }
}
