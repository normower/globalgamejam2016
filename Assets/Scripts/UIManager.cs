﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
public class UIManager : MonoBehaviour
{
    [SerializeField]
    private CanvasGroup BlackScreen, playerUI;
    [SerializeField]
    Text BlackScreenPlayerText, playerName, summaryText, playerFeedback, clueExample;
    [SerializeField]
    private Image inventoryExample;

    private List<Image> inventoryItems;
    private List<Text> clues;


    #region CanvasGroup
    public void FadeInCanvasGroup(CanvasGroup cg)
    {
        EventChecker<CanvasGroup> fadeEvent = new EventChecker<CanvasGroup>(IsFadedIn, EnableCanvasGroup, cg);
        fadeEvent.AddEvent();
    }

    public void FadeOutCanvasGroup(CanvasGroup cg)
    {
        DisableCanvasGroup(cg);
        EventChecker<CanvasGroup> fadeEvent = new EventChecker<CanvasGroup>(IsFadedOut, null, cg);
        fadeEvent.AddEvent();
    }

    private void EnableCanvasGroup(CanvasGroup cg)
    {
        cg.interactable = true;
        cg.blocksRaycasts = true;
    }

    private void DisableCanvasGroup(CanvasGroup cg)
    {
        cg.interactable = false;
        cg.blocksRaycasts = false;
    }

    private bool IsFadedOut(CanvasGroup cg)
    {
        cg.alpha -= Time.deltaTime;

        if (cg.alpha <= 0)
            return true;
        else
            return false;
    }

    private bool IsFadedIn(CanvasGroup cg)
    {
        cg.alpha += Time.deltaTime;

        if (cg.alpha >= 1)
            return true;
        else
            return false;
    }

    #endregion

    #region Text
    public void ShowPlayerFeedback(string text)
    {
        playerFeedback.text = text;        

        if (pulseEvent != null)
            pulseEvent.RemoveEvent();

        Color c = playerFeedback.color;
        c.a = 0;
        playerFeedback.color = c;
        pulseDirection = 1;

        pulseEvent = new EventChecker<Text>(IsPulsed, null, playerFeedback);
        pulseEvent.AddEvent();
    }

    private int pulseDirection = 1;
    private EventChecker<Text> pulseEvent;

    public bool IsPulsed(Text text)
    {
        Color c = text.color;
        c.a += Time.deltaTime * pulseDirection;
        text.color = c;

        if (c.a >= 1)
            pulseDirection = -1;
        else if (c.a <= 0)
            return true;

        return false;
    }

    public void FadeInText(Text text)
    {
        EventChecker<Text> fadeEvent = new EventChecker<Text>(IsFadedIn, null, text);
        fadeEvent.AddEvent();
    }

    public void FadeOutText(Text text)
    {
        EventChecker<Text> fadeEvent = new EventChecker<Text>(IsFadedOut, null, text);
        fadeEvent.AddEvent();
    }

    public bool IsFadedIn(Text text)
    {
        Color c = text.color;
        c.a += Time.deltaTime;
        text.color = c;

        if (c.a >= 1)
            return true;
        else
            return false;
    }

    public bool IsFadedOut(Text text)
    {
        Color c = text.color;
        c.a -= Time.deltaTime;
        text.color = c;

        if (c.a <= 0)
            return true;
        else
            return false;
    }
    #endregion

    #region Room
    public void LeaveRoom(Room room)
    {
        room.RemovePlayer(GameManager.Instance.TurnManager.ReturnCurrentPlayer());
    }

    public void EnterRoom(Room room)
    {
        room.AddPlayer(GameManager.Instance.TurnManager.ReturnCurrentPlayer());
        GameManager.Instance.TurnManager.ReturnCurrentPlayer().CurrentRoom = room;
    }

    public void LoadRoom()
    {
        FadeInCanvasGroup(GameManager.Instance.TurnManager.ReturnCurrentPlayer().CurrentRoom.GetComponent<CanvasGroup>());
        FadeInCanvasGroup(playerUI);
        PopulateInventoryUI();
        PopulateClueUI();
    }
    #endregion

    public void PopulateInventoryUI()
    {
        ClearInventoryUI();
        float xOffset = -100f;

        List<Item> items = GameManager.Instance.TurnManager.ReturnCurrentPlayer().Inventory;

        for (int i = 0; i < items.Count; i++)
        {
            GameObject go = Instantiate(inventoryExample.gameObject) as GameObject;
            go.SetActive(true);
            Image img = go.GetComponent<Image>();
            img.sprite = items[i].image;
            inventoryItems.Add(img);
            RectTransform rt = go.GetComponent<RectTransform>();
            rt.SetParent(inventoryExample.GetComponent<RectTransform>().parent, false);
            Vector3 pos = rt.localPosition;
            pos.x += xOffset * i;
            rt.localPosition = pos;
        }
    }

    private void ClearInventoryUI()
    {
        if (inventoryItems == null)
            inventoryItems = new List<Image>();
        else
        {
            for (int i = 0; i < inventoryItems.Count; i++)
            {
                Destroy(inventoryItems[i].gameObject);
            }
            inventoryItems.Clear();
        }
    }

    public void PopulateClueUI()
    {
        float yOffset = 20f;

        ClearClueUI();

        List<Clue> currentClues = GameManager.Instance.TurnManager.ReturnCurrentPlayer().Clues;

        for (int i = 0; i < currentClues.Count; i++)
        {
            GameObject go = Instantiate(clueExample.gameObject) as GameObject;
            go.SetActive(true);
            Text t = go.GetComponent<Text>();
            t.text = currentClues[i].clueString;
            clues.Add(t);
            RectTransform rt = go.GetComponent<RectTransform>();
            rt.SetParent(clueExample.GetComponent<RectTransform>().parent, false);
            Vector3 pos = rt.localPosition;
            pos.y += yOffset * i;
            rt.localPosition = pos;
        }
    }

    private void ClearClueUI()
    {
        if (clues == null)
            clues = new List<Text>();
        else
        {
            for (int i = 0; i < clues.Count; i++)
            {
                Destroy(clues[i].gameObject);
            }
            clues.Clear();
        }


    }

    public void Wait()
    {
        NextTurn();
    }

    public void NextTurn()
    {
        FadeOutCanvasGroup(playerUI);
        FadeOutCanvasGroup(GameManager.Instance.TurnManager.ReturnCurrentPlayer().CurrentRoom.GetComponent<CanvasGroup>());
        GameManager.Instance.TurnManager.NextPlayer();        
    }
    
    public void SetPlayerText(Text t)
    {
        string s = "";
        switch(GameManager.Instance.TurnManager.ReturnCurrentPlayer().ThisPlayerType)
        {
            case PlayerType.Jock:
            s = "It is Chad the Jock's turn.";
            break;
            case PlayerType.Joker:
            s = "It is Joker the Joker's turn.";
            break;
            case PlayerType.Slut:
            s = "It is Scott the Slut's turn.";
            break;
            case PlayerType.Virgin:
            s = "It is Mary the Virgin's turn.";
            break;
        }
        BlackScreenPlayerText.text = s;
        SetPlayerNameText();
    }    
    public void SetPlayerNameText()
    {
         string s = "";
        switch(GameManager.Instance.TurnManager.ReturnCurrentPlayer().ThisPlayerType)
        {
            case PlayerType.Jock:
            s = "Chad the Jock";
            break;
            case PlayerType.Joker:
            s = "Joker the Joker";
            break;
            case PlayerType.Slut:
            s = "Scott the Slut";
            break;
            case PlayerType.Virgin:
            s = "Mary the Virgin";
            break;
        }
        playerName.text = s;
    }

    public void UpdateSummaryText(string text)
    {
        summaryText.text = text;
    }
}
