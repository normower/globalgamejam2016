﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

public class TurnManager
{
    List<Player> playerList;
    int currentPlayer, possessedPlayer, turnCount = 1;
    public bool summaryActive;

    public int CurrentPlayer()
    {
        return currentPlayer;
    }
    
    public int PossessedPlayer()
    {
        return possessedPlayer;
    }
    
    
    public TurnManager()
    {
        playerList = new List<Player>();
        playerList.Add(new Player(RoomType.Entrance, PlayerType.Jock));
        playerList.Add(new Player(RoomType.Entrance, PlayerType.Joker));
        playerList.Add(new Player(RoomType.Entrance, PlayerType.Slut));
        playerList.Add(new Player(RoomType.Entrance, PlayerType.Virgin));
        
    }
    public void NextPlayer()
    {
        string summaryText = "";

        if (summaryActive)
            summaryActive = false;

        currentPlayer++;
        Debug.Log(currentPlayer);
        if(currentPlayer > (playerList.Count-1))
        {
            currentPlayer = 0;
            turnCount++;
            if (turnCount > 1)
            {
                if (GameManager.Instance.RitualManager.CurrentRitual == null)
                {
                    summaryText = "The Ritual has Begun";
                    GameManager.Instance.RitualManager.CreateRitual(ReturnPlayerByType(ReturnRandomPlayerType()));
                }
                else
                {
                    if (GameManager.Instance.TaskManager.CheckTaskComplete(GameManager.Instance.RitualManager.CurrentRitual.TaskList[0]))
                    {
                        Debug.Log("Ritual Complete!");
                        SceneManager.LoadScene(3);
                    }
                    else if (GameManager.Instance.TaskManager.CheckTaskComplete(GameManager.Instance.RitualManager.CurrentRitual.CounterTaskList[0]))
                    {
                        Debug.Log("Counter Complete!");
                        SceneManager.LoadScene(4);
                    }

                    summaryText = RitualContinuesText();
                }
            }
            else
                summaryText = RitualHasntStartedText();
        }

        GameManager.Instance.UIManager.UpdateSummaryText(summaryText);
    }
    
    public void SetCurrentPlayerPossessed()
    {
        possessedPlayer = currentPlayer;
    }
    
    public Player ReturnCurrentPlayer()
    {
        return playerList[currentPlayer];
    }
    
    public PlayerType ReturnRandomPlayerType()
    {
        var v = PlayerType.GetValues(typeof(PlayerType));
        return (PlayerType) v.GetValue(new System.Random().Next(v.Length));
    }
    
    public Player ReturnPlayerByType(PlayerType t)
    {
        Debug.Log("type: " + t);
        for(int i = 0; i < playerList.Count; i++)
        {
            if(playerList[i].ThisPlayerType == t)
            {
                return playerList[i];
            }
        }
        return null;
    }
    public bool IsPlayerInRoom(PlayerType t, RoomType r)
    {
        bool b = false;
        for(int i = 0; i < playerList.Count; i++)
        {
            if(playerList[i].ThisPlayerType == t)
            {
                if(playerList[i].CurrentRoomType == r)
                {
                    b = true;
                }
            }
        }
        return b;
    }
    
    private string RitualContinuesText()
    {
        string[] text = new string[]
        {
            "One of you lies...",
            "It'll all be over soon...",
        };

        return text[Random.Range(0, text.Length - 1)];
    }

    private string RitualHasntStartedText()
    {
        string[] text = new string[]
        {
            "The air feels heavy",
            "Where's that pesky key",
        };

        return text[Random.Range(0, text.Length - 1)];
    }
}
