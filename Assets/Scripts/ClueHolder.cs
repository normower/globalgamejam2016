﻿using UnityEngine;
using System.Collections;

public class ClueHolder : MonoBehaviour {

	public Clue clue;

    public void PickUp()
    {
        if (string.IsNullOrEmpty(clue.clueString) || GameManager.Instance.TurnManager.ReturnCurrentPlayer().Possessed)
        {
            GameManager.Instance.UIManager.ShowPlayerFeedback("Picked up Nothing");
            return;
        }

        if (GameManager.Instance.TurnManager.ReturnCurrentPlayer().Possessed)
        {
            GameManager.Instance.UIManager.ShowPlayerFeedback("A Clue! It burns your hand when you try to touch it");
            return;
        }

        GameManager.Instance.TurnManager.ReturnCurrentPlayer().Clues.Add(clue);
        GameManager.Instance.UIManager.ShowPlayerFeedback(clue.clueString);
        GameManager.Instance.UIManager.PopulateClueUI();
        clue = new Clue("");
    }

    private void Awake()
    {
        clue = new Clue("");
        GameManager.Instance.ClueManager.AddItem(this);
    }
	
}
