using UnityEngine;
using System.Collections;

[RequireComponent(typeof(ItemManager))]
[RequireComponent(typeof(ClueManager))]
[RequireComponent(typeof(UIManager))]
public class GameManager : MonoBehaviour
{
    private static GameManager instance;
    private ItemManager itemManager;
    private TaskManager taskManager;
    private RitualManager ritualManager;
    private ClueManager clueManager;
    private EventManager eventManager;
    private TurnManager turnManager;
    private RoomManager roomManager;
    private UIManager uiManager;
    #region Accessors

    public static GameManager Instance
    {
        get { return instance; }
        private set
        {
            if (instance != null)
                Destroy(value);
            else
                instance = value;
        }
    }

    public ItemManager ItemManager
    {
        get { return itemManager; }
    }
    
    public TaskManager TaskManager
    {
        get{ 
        if(taskManager == null)
        {
            taskManager = new TaskManager();
        }    
        return taskManager; }
    }
    public RitualManager RitualManager
    {
        get{
            if(ritualManager == null)
            {
                ritualManager = new RitualManager();
            }
            return ritualManager;
        }
    }
    public ClueManager ClueManager
    {
        get { return clueManager;}
    }

    public EventManager EventManager
    {
        get { return eventManager; }
    }

    public TurnManager TurnManager
    {
        get {
            if(turnManager == null)
            {
                turnManager = new TurnManager();
            }
            return turnManager;}
    }
    
    public RoomManager RoomManager
    {
        get {
            if(roomManager == null)
            {
                roomManager = new RoomManager();
            }
            return roomManager;
        }
    }

    public UIManager UIManager
    {
        get { return uiManager; }
    }
    
    #endregion

    private void Update()
    {
        eventManager.Update();
    }

    private void Start()
    {
        itemManager.AssignItems();
        
        Debug.Log("CurrentPlayer: "+TurnManager.CurrentPlayer());
        GameManager.instance.UIManager.LoadRoom();
    }

    private void Awake()
    {
        Instance = this;

        itemManager = GetComponent<ItemManager>();
        clueManager = GetComponent<ClueManager>();
        uiManager = GetComponent<UIManager>();
        eventManager = new EventManager();
    }
}
